<?php
include_once "locale.php";

class Lib
{
    private  $host = 'localhost:3306';
    private  $dbname = 'chatm';
    private  $login = 'phpmyadmin';
    private  $password = 'some_pass';
    private  $charset = 'utf8';
    private  $connection;

    function get_Data_DB ($query)
    {
        $this->connection = "mysql:host=$this->host;dbname=$this->dbname;charset=$this->charset";
        try
        {
            $db = new PDO($this->connection, $this->login, $this->password);
        }
        catch (PDOException $e)
        {
            echo $e->getMessage();
        }
        $stmt = $db->prepare($query);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
        {
            $out[] = $row;
        }
        return $out ? $out : false;
    }

    public function addUser($chatId, $name)
    {
        $res = $this->get_Data_DB("SELECT `name` FROM chatm.users WHERE `chat_id` = '".$chatId."'");
        if (is_null($res[0]['name'])){
            $this->get_Data_DB("INSERT INTO `chatm`.`users` (`chat_id`, `name`, `date_add`) VALUES ('".$chatId."', '".$name."', '".time()."')");
        }
    }

    public function setLastAction($chatId, $lastAction){
        $res = $this->get_Data_DB("UPDATE `chatm`.`users` SET last_action = '".$lastAction."' WHERE `chat_id` = '".$chatId."'");
        return $res;
    }

    public function getLastAction($chatId){
        $res = $this->get_Data_DB("SELECT `last_action` FROM chatm.users WHERE `chat_id` = '".$chatId."'");
        return $res[0]['last_action'];
    }

    public function getQuestion($id, $number = 'q1'){
        return $this->get_Data_DB("SELECT `".$number."` FROM `active` WHERE `id` = '".$id."'")[0][$number];
    }

    public function getActiv($name){
        global $l;
        switch ($name) {
            case $l['inetMagaz']:
                $data = '1';
                break;

            case $l['dost']:
                $data = '2';
                break;

            case $l['uslygi']:
                $data = '3';
                break;

            case $l['restor']:
                $data = '5';
                break;

            case $l['obraz']:
                $data = '4';
                break;
        }

        return $data;
    }

    public function setBrif($chatId, $data, $type){

        $res = $this->get_Data_DB("SELECT `activity` FROM `brif` WHERE `chat_id` = '".$chatId."'");
        if (is_null($res) || !$res){
            $this->get_Data_DB("INSERT INTO `brif` (`".$type."`, `chat_id`) VALUES ('".$data."', '".$chatId."')");
        } else {
            $this->get_Data_DB("UPDATE `brif` SET ".$type." = '".$data."' WHERE `chat_id` = '".$chatId."'");
        }
    }

    public function setUserInfo($chatId, $field, $data){
        $this->get_Data_DB("UPDATE `chatm`.`users` SET ".$field." = '".$data."' WHERE `chat_id` = '".$chatId."'");
    }

    public function getUserInfo($field, $chatId){
        $res = $this->get_Data_DB("SELECT `".$field."` FROM chatm.users WHERE `chat_id` = '".$chatId."'")[0][$field];
        return $res;
    }

    public function getAllUserInfo($chatId){
        $res = $this->get_Data_DB("SELECT `name`, `number`, `email` FROM chatm.users WHERE `chat_id` = '".$chatId."'")[0];
        return $res;
    }

    public function getAction(){
        $res = $this->get_Data_DB("SELECT `type` FROM chatm.active");
        return $res;
    }

    public function sendMail($chatId, $type = "facebook test")
    {
        $this->get_Data_DB("UPDATE `users` SET orders = '1' WHERE `chat_id` = '" . $chatId . "'");
        $res = $this->get_Data_DB("SELECT `activity` FROM chatm.brif WHERE `chat_id` = '" . $chatId . "'")[0]['activity'];
        $user = $this->get_Data_DB("SELECT `name`, `number`, `email` FROM chatm.users WHERE `chat_id` = '" . $chatId . "'")[0];
        $activity = $this->get_Data_DB("SELECT `type` FROM chatm.active WHERE `id` = '" . $res . "'")[0];
        $qwe = $this->get_Data_DB("SELECT `q1`, `q2`, `q3`, `q4`, `q5` FROM chatm.brif WHERE `chat_id` = '" . $chatId . "'")[0];
        $i = 1;
        $mes = "";
        foreach ($qwe as $value) {
            $text = $this->get_Data_DB("SELECT `q" . $i . "` FROM chatm.active WHERE `id` = '" . $res . "'")[0];
            $l = "q" . $i;
            $q = $value[$l] == "1" ? "Да" : "Нет";
            $text = $text[$l] ? $text[$l] : 'nope';
            if ($text == 'nope'){
                $i++;
            } else {
                $mes .= $text . " : " . $q . "\n";
                $i++;
            }
        }
        $type = $type == 'call' ? "Позвонить" : "Написать";
//        $data = [
//            'email' => $user['email'],
//            'phone' => $user['number'],
//        ];
//
//        $options = [
//            CURLOPT_URL => 'http://bot.delive.me/chatM_bot/telegram/lib/test.php',
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_POST => true,
//            CURLOPT_POSTFIELDS => $data,
//        ];

        $curl = curl_init();
//        curl_setopt_array($curl, $options);
//        $res = curl_exec($curl);

        $chatId = $this->get_Data_DB("SELECT `chat_id` FROM `chatm`.`users` WHERE `flow`='1'");

        foreach ($chatId as $value){
            foreach ($value as $item) {
                $chats[]= $item;
            }
        }

        $bron = [
            'Тип ' => $type ? $type." test facebook" : 'nope',
            'Клиент ' => $user['name'] ? $user['name'] : 'nope',
            'Выбранная сфера ' => $activity['type'] ? $activity['type'] : 'nope',
            'Бриф ' => $mes ? $mes : 'nope',
            'Контактный телефон ' => $user['number'] ? $user['number'] : 'nope',
            'Контактный email ' => $user['email'] ? $user['email'] : 'nope',
        ];

        $data = [
            'send' => '1',
            'chatid' => json_encode($chats),
            'data' => json_encode($bron),
        ];

        $options = [
            CURLOPT_URL => 'https://bot.delive.me/chatM_bot/telegram/admin/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data,
        ];

        curl_setopt_array($curl, $options);
        curl_exec($curl);
        curl_close($curl);

    }


    public function checkOrder($chatId){
        $res = $this->get_Data_DB("SELECT `orders` FROM chatm.users WHERE `chat_id` = '".$chatId."'")[0]['orders'];
        return $res;
    }

    public function setAnswer($id, $text, $chatId){
        $this->get_Data_DB("INSERT INTO `chatm`.`answers` (`id_int`, `text`, `chat_id`) VALUES ('".$id."', '".$text."', '".$chatId."')");
    }


    public function getAllUsers(){
        $res = $this->get_Data_DB("SELECT `chat_id` FROM chatm.users WHERE `platform` = '1'");
        return $res;
    }
}
?>


