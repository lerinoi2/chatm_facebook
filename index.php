<?php

$verify_token = "qwerty100";
$token = "EAAd1LXom8noBAN3CCPwkvMucSN9bZAUENwnytlYtn7SZAEgZCSsmBRzPwrZBZBUsRVDCbESCWWkpKBu3Kyy8ihgj52y9RfLlE6XpZC3fxLUpDskq3skvCXDLIAWpkR3iOT5C2dCeD8LP8lmC5OZCiAtxbsFaZAZBikXseVIlMdRsiLZAmgDKdsf3ZAz";

if (file_exists(__DIR__ . '/config.php')) {
    $config = include __DIR__ . '/config.php';
    $verify_token = $config['verify_token'];
    $token = $config['token'];
}

require_once(dirname(__FILE__) . '/vendor/autoload.php');
require_once(dirname(__FILE__) . '/lib/market.php');
require_once(dirname(__FILE__) . '/lib/locale.php');

use pimax\FbBotApp;
use pimax\Menu\MenuItem;
use pimax\Menu\LocalizedMenu;
use pimax\Messages\Message;
use pimax\Messages\MessageButton;
use pimax\Messages\StructuredMessage;
use pimax\Messages\MessageElement;
use pimax\Messages\MessageReceiptElement;
use pimax\Messages\Address;
use pimax\Messages\Summary;
use pimax\Messages\Adjustment;
use pimax\Messages\AccountLink;
use pimax\Messages\ImageMessage;
use pimax\Messages\QuickReply;
use pimax\Messages\QuickReplyButton;
use pimax\Messages\SenderAction;

$lib = new Lib();


$bot = new FbBotApp($token);
if (!empty($_REQUEST['local'])) {
    $message = new ImageMessage(1585388421775947, dirname(__FILE__).'/fb4d_logo-2x.png');
    $message_data = $message->getData();
    $message_data['message']['attachment']['payload']['url'] = 'fb4d_logo-2x.png';
    echo '<pre>', print_r($message->getData()), '</pre>';
    $res = $bot->send($message);
    echo '<pre>', print_r($res), '</pre>';
}
// Receive something
if (!empty($_REQUEST['hub_mode']) && $_REQUEST['hub_mode'] == 'subscribe' && $_REQUEST['hub_verify_token'] == $verify_token) {
    // Webhook setup request
    echo $_REQUEST['hub_challenge'];
} else {
    // Other event
    $data = json_decode(file_get_contents("php://input"), true, 512, JSON_BIGINT_AS_STRING);

    if (!empty($data['entry'][0]['messaging'])) {
        foreach ($data['entry'][0]['messaging'] as $message) {

            // Skipping delivery messages
            if (!empty($message['delivery'])) {
                continue;
            }
            // skip the echo of my own messages
            if (($message['message']['is_echo'] == "true")) {
                continue;
            }
            $command = "";
            // When bot receive message from user
            if (!empty($message['message'])) {
                $command = trim($message['message']['text']);
                // When bot receive button click from user
            } else if (!empty($message['postback'])) {

                switch (trim($message['postback']['payload'])){

                    case "PAYLOAD - get started button":
                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_ON));
                        $lib->setLastAction($message['sender']['id'], "");
                        $user = $bot->userProfile($message['sender']['id']);
                        $name = $user->getFirstName()." ".$user->getLastName();
                        $lib->addUser($message['sender']['id'], $name);
                        $res = $lib->getUserInfo('number', $message['sender']['id']);
                        if (is_null($res)) {
                            $bot->send(new Message($message['sender']['id'], "Пожалуйста, оставьте Ваши контакты, чтобы чат-бот мог внести Вас в список своих пользователей."));
                            $lib->setLastAction($message['sender']['id'], 'number_start');
                        }
                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_OFF));
                    break;

                    case $l['whatCan']:
                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_ON));
                        $bot->send(new Message($message['sender']['id'], $l['iCan']));
                        $bot->send(new StructuredMessage($message['sender']['id'],
                            StructuredMessage::TYPE_BUTTON,
                            [
                                'text' => $l['goWork'],
                                'buttons' => [
                                    new MessageButton(MessageButton::TYPE_POSTBACK, $l['goBrif']),
                                    new MessageButton(MessageButton::TYPE_POSTBACK, $l['moreQuestions']),
                                ]
                            ]
                        ));
                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_OFF));
                    break;

                    case $l['goBrif']:
                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_ON));
                        $res = $lib->checkOrder($message['sender']['id']);
                        if ($res == '1'){
                            $bot->send(new StructuredMessage($message['sender']['id'],
                                StructuredMessage::TYPE_BUTTON,
                                [
                                    'text' => 'Хотите изменить бриф?',
                                    'buttons' => [
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['yes']),
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['no']),
                                    ]
                                ]
                            ));
                            $lib->setLastAction($message['sender']['id'], json_encode(array('name' => 'changeBrif')));
                            break;
                        } else {
                            $bot->send(new StructuredMessage($message['sender']['id'],
                                StructuredMessage::TYPE_GENERIC,
                                [
                                    'elements' => [
                                        new MessageElement( "Вид деятельности компании?", "", "", [
                                            new MessageButton(MessageButton::TYPE_POSTBACK, $l['inetMagaz']),
                                            new MessageButton(MessageButton::TYPE_POSTBACK, $l['dost']),
                                            new MessageButton(MessageButton::TYPE_POSTBACK, $l['uslygi']),
                                        ]),
                                        new MessageElement( "Вид деятельности компании?", "", "", [
                                            new MessageButton(MessageButton::TYPE_POSTBACK, $l['restor']),
                                            new MessageButton(MessageButton::TYPE_POSTBACK, $l['obraz'])
                                        ])
                                    ]
                                ]
                            ));
                            $lib->setLastAction($message['sender']['id'], json_encode(array('name' => 'activity')));
                        }
                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_OFF));
                    break;

                    case $l['moreQuestions']:
                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_ON));
                        $bot->send(new Message($message['sender']['id'], $l['chtoras']));

                        $bot->send(new StructuredMessage($message['sender']['id'],
                            StructuredMessage::TYPE_GENERIC,
                            [
                                'elements' => [
                                    new MessageElement( $l['where'], "", "", [
                                        new MessageButton(MessageButton::TYPE_POSTBACK, 'Узнать', $l['where']),
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['back']),
                                    ])
                                ]
                            ]
                        ));

                        $bot->send(new StructuredMessage($message['sender']['id'],
                            StructuredMessage::TYPE_GENERIC,
                            [
                                'elements' => [
                                    new MessageElement( $l['etap'], "", "", [
                                        new MessageButton(MessageButton::TYPE_POSTBACK, 'Узнать', $l['etap']),
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['back']),
                                    ])
                                ]
                            ]
                        ));

                        $bot->send(new StructuredMessage($message['sender']['id'],
                            StructuredMessage::TYPE_GENERIC,
                            [
                                'elements' => [
                                    new MessageElement( $l['howMatch'], "", "", [
                                        new MessageButton(MessageButton::TYPE_POSTBACK, 'Узнать', $l['howMatch']),
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['back']),
                                    ])
                                ]
                            ]
                        ));

                        $bot->send(new StructuredMessage($message['sender']['id'],
                            StructuredMessage::TYPE_GENERIC,
                            [
                                'elements' => [
                                    new MessageElement( $l['dov'], "", "", [
                                        new MessageButton(MessageButton::TYPE_POSTBACK, 'Узнать', $l['dov']),
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['back']),
                                    ])
                                ]
                            ]
                        ));

//                        $bot->send(new StructuredMessage($message['sender']['id'],
//                            StructuredMessage::TYPE_GENERIC,
//                            [
//                                'elements' => [
//                                    new MessageElement( $l['chtoras'], "", "", [
//                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['where']),
//                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['etap']),
//                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['howMatch']),
//                                    ]),
//                                    new MessageElement( $l['chtoras'], "", "", [
//                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['dov']),
//                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['back'])
//                                    ])
//                                ]
//                            ]
//                        ));

                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_OFF));
                    break;

                    case $l['where']:
                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_ON));

                        $bot->send(new Message($message['sender']['id'], $l['whereText']));
                        $bot->send(new Message($message['sender']['id'], $l['inetMagaz']."\n\n".$l['dost']."\n\n".$l['uslygi']."\n\n".$l['restor']."\n\n".$l['obraz']));

                        $bot->send(new StructuredMessage($message['sender']['id'],
                            StructuredMessage::TYPE_GENERIC,
                            [
                                'elements' => [
                                    new MessageElement( 'Меню', "", "", [
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['where']),
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['etap']),
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['howMatch']),
                                    ]),
                                    new MessageElement( 'Меню', "", "", [
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['dov']),
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['back'])
                                    ])
                                ]
                            ]
                        ));
                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_OFF));
                    break;

                    case $l['dov']:
                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_ON));
                        $bot->send(new Message($message['sender']['id'], $l['dovText']));
                        $bot->send(new Message($message['sender']['id'], $l['dovText2']));
                        $bot->send(new StructuredMessage($message['sender']['id'],
                            StructuredMessage::TYPE_GENERIC,
                            [
                                'elements' => [
                                    new MessageElement( 'Меню', "", "", [
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['where']),
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['etap']),
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['howMatch']),
                                    ]),
                                    new MessageElement( 'Меню', "", "", [
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['dov']),
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['back'])
                                    ])
                                ]
                            ]
                        ));
                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_OFF));
                    break;

                    case $l['etap']:
                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_ON));
                        $bot->send(new Message($message['sender']['id'], $l['etapText']));
                        $bot->send(new StructuredMessage($message['sender']['id'],
                            StructuredMessage::TYPE_GENERIC,
                            [
                                'elements' => [
                                    new MessageElement( 'Меню', "", "", [
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['where']),
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['etap']),
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['howMatch']),
                                    ]),
                                    new MessageElement( 'Меню', "", "", [
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['dov']),
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['back'])
                                    ])
                                ]
                            ]
                        ));
                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_OFF));
                    break;

                    case $l['howMatch']:
                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_ON));
                        $bot->send(new Message($message['sender']['id'],'Базовый чат-бот стоит от 15 000 р.'));
                        $bot->send(new Message($message['sender']['id'], $l['howMatchText']));
                        $bot->send(new StructuredMessage($message['sender']['id'],
                            StructuredMessage::TYPE_BUTTON,
                            [
                                'text' => $l['goBrifi'],
                                'buttons' => [
                                    new MessageButton(MessageButton::TYPE_POSTBACK, $l['yes']),
                                    new MessageButton(MessageButton::TYPE_POSTBACK, $l['no']),
                                ]
                            ]
                        ));
                        $lib->setLastAction($message['sender']['id'], json_encode(array('name' => 'brif')));
                        $bot->send(new StructuredMessage($message['sender']['id'],
                            StructuredMessage::TYPE_GENERIC,
                            [
                                'elements' => [
                                    new MessageElement( 'Меню', "", "", [
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['where']),
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['etap']),
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['howMatch']),
                                    ]),
                                    new MessageElement( 'Меню', "", "", [
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['dov']),
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['back'])
                                    ])
                                ]
                            ]
                        ));
                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_OFF));
                    break;

                    case $l['back']:
                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_ON));
                        $bot->send(new Message($message['sender']['id'],'Назад'));
                        $bot->send(new StructuredMessage($message['sender']['id'],
                            StructuredMessage::TYPE_GENERIC,
                            [
                                'elements' => [
                                    new MessageElement( 'Меню', "", "", [
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['whatCan']),
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['order'])
                                    ]),
                                    new MessageElement( 'Меню', "", "", [
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['price']),
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['callMe'])
                                    ])
                                ]
                            ]
                        ));
                        $lib->setLastAction($message['sender']['id'], "");
                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_OFF));
                    break;

                    case $l['callMe']:
                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_ON));
                        $bot->send(new StructuredMessage($message['sender']['id'],
                            StructuredMessage::TYPE_BUTTON,
                            [
                                'text' => $l['callMeText'],
                                'buttons' => [
                                    new MessageButton(MessageButton::TYPE_POSTBACK, $l['callm']),
                                    new MessageButton(MessageButton::TYPE_POSTBACK, $l['textm']),
                                ]
                            ]
                        ));
                        $lib->setLastAction($message['sender']['id'], json_encode(array('name' => 'call')));
                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_OFF));
                    break;

                    case $l['order']:
                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_ON));
                        $bot->send(new Message($message['sender']['id'], $l['goBrifi']));
                        $bot->send(new StructuredMessage($message['sender']['id'],
                            StructuredMessage::TYPE_BUTTON,
                            [
                                'text' => $l['callMeText'],
                                'buttons' => [
                                    new MessageButton(MessageButton::TYPE_POSTBACK, $l['callm']),
                                    new MessageButton(MessageButton::TYPE_POSTBACK, $l['textm']),
                                ]
                            ]
                        ));
                        $lib->setLastAction($chatId, ' ');
                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_OFF));
                    break;

                    case $l['price']:
                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_ON));
                        $bot->send(new Message($message['sender']['id'], 'Минимальная стоимость чат-бота - 15 000 р.'));
                        $bot->send(new StructuredMessage($message['sender']['id'],
                            StructuredMessage::TYPE_BUTTON,
                            [
                                'text' => $l['priceText'],
                                'buttons' => [
                                    new MessageButton(MessageButton::TYPE_POSTBACK, 'Продолжить'),
                                    new MessageButton(MessageButton::TYPE_POSTBACK, 'Свяжитесь со мной'),
                                ]
                            ]
                        ));
                        $lib->setLastAction($message['sender']['id'], json_encode(array('name' => 'price')));
                        $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_OFF));
                    break;

                    default:
                        $action = json_decode($lib->getLastAction($message['sender']['id']));
                        if ($action) {
                            switch ($action->name) {
                                case "activity":
                                    $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_ON));
                                    $id = $lib->getActiv(trim($message['postback']['payload']));
                                    $lib->setBrif($message['sender']['id'], $id, 'activity');
                                    $bot->send(new Message($message['sender']['id'], "На дальнейшие вопросы отвечайте односложно - да или нет."));
                                    $mes = $lib->getQuestion($id);
                                    $bot->send(new StructuredMessage($message['sender']['id'],
                                        StructuredMessage::TYPE_BUTTON,
                                        [
                                            'text' => $mes,
                                            'buttons' => [
                                                new MessageButton(MessageButton::TYPE_POSTBACK, $l['yes']),
                                                new MessageButton(MessageButton::TYPE_POSTBACK, $l['no']),
                                            ]
                                        ]
                                    ));

                                    $lib->setLastAction($message['sender']['id'], json_encode(array('name' => 'answers', 'question' => 1, 'type' => $id)));

                                    $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_OFF));
                                    break;

                                case "answers":
                                    $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_ON));

                                    $answer = trim($message['postback']['payload']) == $l['yes'] ? '1' : '2';

                                    $question = 'q'.$action->question;
                                    $lib->setBrif($message['sender']['id'], $answer, $question);
                                    $question_next =$action->question + 1;
                                    $mes = $lib->getQuestion($action->type, 'q'.$question_next);

                                    $bot->send(new StructuredMessage($message['sender']['id'],
                                        StructuredMessage::TYPE_BUTTON,
                                        [
                                            'text' => $mes,
                                            'buttons' => [
                                                new MessageButton(MessageButton::TYPE_POSTBACK, $l['yes']),
                                                new MessageButton(MessageButton::TYPE_POSTBACK, $l['no']),
                                            ]
                                        ]
                                    ));

                                    if ($action->question == 4 and $action->type != 1){
                                        $lib->setLastAction($message['sender']['id'], json_encode(array('name' => 'answers5', 'question' => $question_next, 'type' => $action->type)));
                                    } elseif ($action->question == 3 and $action->type == 1) {
                                        $lib->setLastAction($message['sender']['id'], json_encode(array('name' => 'answers5', 'question' => $question_next, 'type' => $action->type)));
                                    } else {
                                        $lib->setLastAction($message['sender']['id'], json_encode(array('name' => 'answers', 'question' => $question_next, 'type' => $action->type)));
                                    }
                                    $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_OFF));
                                break;

                                case "answers5":
                                    $answer = trim($message['postback']['payload']) == $l['yes'] ? '1' : '2';
                                    $lib->setBrif($message['sender']['id'], $answer, 'q4');
                                    $res = $lib->getUserInfo('number', $message['sender']['id']);
                                    if (is_null($res)){
                                        $bot->send(new Message($message['sender']['id'],"Нам очень важны Ваши ответы. Пожалуйста, оставьте Ваши контакты, а мы заботливо сохраним их, чтобы уточнить информацию."));
                                        $lib->setLastAction($message['sender']['id'], 'number');
                                    } else {
                                        $bot->send(new Message($message['sender']['id'], $l['aleksandr']));
                                        $bot->send(new StructuredMessage($message['sender']['id'],
                                            StructuredMessage::TYPE_GENERIC,
                                            [
                                                'elements' => [
                                                    new MessageElement("Меню", "", "", [
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['whatCan']),
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['order'])
                                                    ]),
                                                    new MessageElement("Меню", "", "", [
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['price']),
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['callMe'])
                                                    ])
                                                ]
                                            ]
                                        ));
                                        $lib->setLastAction($message['sender']['id'], ' ');
                                        $lib->sendMail($message['sender']['id']);
                                    }

                                break;

                                case "brif":
                                    $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_ON));
                                    $answer = trim($message['postback']['payload']) == $l['yes'] ? '1' : '2';
                                    if ($answer == 2){
                                        $bot->send(new Message($message['sender']['id'], "О чем рассказать?\nВыберите тему, пожалуйста! 👐"));
                                        $bot->send(new StructuredMessage($message['sender']['id'],
                                            StructuredMessage::TYPE_GENERIC,
                                            [
                                                'elements' => [
                                                    new MessageElement( 'Меню', "", "", [
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['where']),
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['etap']),
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['howMatch']),
                                                    ]),
                                                    new MessageElement( 'Меню', "", "", [
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['dov']),
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['back'])
                                                    ])
                                                ]
                                            ]
                                        ));
                                        break;
                                    }
                                    $res = $lib->checkOrder($message['sender']['id']);
                                    if ($res == '1'){
                                        $bot->send(new StructuredMessage($message['sender']['id'],
                                            StructuredMessage::TYPE_BUTTON,
                                            [
                                                'text' => 'Хотите изменить бриф?',
                                                'buttons' => [
                                                    new MessageButton(MessageButton::TYPE_POSTBACK, $l['yes']),
                                                    new MessageButton(MessageButton::TYPE_POSTBACK, $l['no']),
                                                ]
                                            ]
                                        ));
                                    } else {
                                        $bot->send(new StructuredMessage($message['sender']['id'],
                                            StructuredMessage::TYPE_GENERIC,
                                            [
                                                'elements' => [
                                                    new MessageElement( "Вид деятельности компании?", "", "", [
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['inetMagaz']),
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['dost']),
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['uslygi']),
                                                    ]),
                                                    new MessageElement( "Вид деятельности компании?", "", "", [
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['restor']),
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['obraz'])
                                                    ])
                                                ]
                                            ]
                                        ));
                                        $lib->setLastAction($message['sender']['id'], json_encode(array('name' => 'activity')));
                                    }
                                    $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_OFF));
                                break;

                                case "call":
                                    $answer = trim($message['postback']['payload']);
                                    if ($answer == $l['callm']){

                                        $res = $lib->getUserInfo('number', $message['sender']['id']);
                                        if (is_null($res)) {
                                            $bot->send(new Message($message['sender']['id'], "Уважаю классику!"));
                                            $bot->send(new Message($message['sender']['id'], $l['gophone']));
                                            $lib->setLastAction($message['sender']['id'], 'number');
                                        } else {
                                            $lib->sendMail($message['sender']['id'], "call");
                                            $bot->send(new Message($message['sender']['id'], $l['aleksandr2']));
                                        }

                                    } else {
                                        $res = $lib->getUserInfo('number', $message['sender']['id']);
                                        if (is_null($res)) {
                                            $bot->send(new Message($message['sender']['id'], $l['gophone']));
                                            $lib->setLastAction($message['sender']['id'], 'number');
                                        } else {
                                            $lib->sendMail($message['sender']['id'], "text");
                                            $bot->send(new Message($message['sender']['id'], $l['aleksandr2']));
                                        }
                                    }
                                    $bot->send(new StructuredMessage($message['sender']['id'],
                                        StructuredMessage::TYPE_GENERIC,
                                        [
                                            'elements' => [
                                                new MessageElement( 'Меню', "", "", [
                                                    new MessageButton(MessageButton::TYPE_POSTBACK, $l['whatCan']),
                                                    new MessageButton(MessageButton::TYPE_POSTBACK, $l['order'])
                                                ]),
                                                new MessageElement( 'Меню', "", "", [
                                                    new MessageButton(MessageButton::TYPE_POSTBACK, $l['price']),
                                                    new MessageButton(MessageButton::TYPE_POSTBACK, $l['callMe'])
                                                ])
                                            ]
                                        ]
                                    ));
                                break;

                                case "price":
                                    $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_ON));
                                    $answer = trim($message['postback']['payload']);
                                    if ($answer == 'Продолжить'){
                                        $res = $lib->checkOrder($message['sender']['id']);
                                        if ($res == '1'){
                                            $bot->send(new StructuredMessage($message['sender']['id'],
                                                StructuredMessage::TYPE_BUTTON,
                                                [
                                                    'text' => 'Хотите изменить бриф?',
                                                    'buttons' => [
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['yes']),
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['no']),
                                                    ]
                                                ]
                                            ));
                                        } else {
                                            $bot->send(new StructuredMessage($message['sender']['id'],
                                                StructuredMessage::TYPE_GENERIC,
                                                [
                                                    'elements' => [
                                                        new MessageElement( "Вид деятельности компании?", "", "", [
                                                            new MessageButton(MessageButton::TYPE_POSTBACK, $l['inetMagaz']),
                                                            new MessageButton(MessageButton::TYPE_POSTBACK, $l['dost']),
                                                            new MessageButton(MessageButton::TYPE_POSTBACK, $l['uslygi']),
                                                        ]),
                                                        new MessageElement( "Вид деятельности компании?", "", "", [
                                                            new MessageButton(MessageButton::TYPE_POSTBACK, $l['restor']),
                                                            new MessageButton(MessageButton::TYPE_POSTBACK, $l['obraz'])
                                                        ])
                                                    ]
                                                ]
                                            ));
                                            $lib->setLastAction($message['sender']['id'], json_encode(array('name' => 'activity')));
                                        }
                                    }
                                    else {
                                        $bot->send(new StructuredMessage($message['sender']['id'],
                                            StructuredMessage::TYPE_BUTTON,
                                            [
                                                'text' => $l['callMeText'],
                                                'buttons' => [
                                                    new MessageButton(MessageButton::TYPE_POSTBACK, $l['callm']),
                                                    new MessageButton(MessageButton::TYPE_POSTBACK, $l['textm']),
                                                ]
                                            ]
                                        ));
                                        $lib->setLastAction($message['sender']['id'], json_encode(array('name' => 'call')));
                                    }
                                    $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_OFF));
                                break;

                                case "changeBrif":
                                    $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_ON));
                                    $answer = trim($message['postback']['payload']);
                                    if ($answer == $l['yes']){
                                        $bot->send(new StructuredMessage($message['sender']['id'],
                                            StructuredMessage::TYPE_GENERIC,
                                            [
                                                'elements' => [
                                                    new MessageElement( "Вид деятельности компании?", "", "", [
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['inetMagaz']),
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['dost']),
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['uslygi']),
                                                    ]),
                                                    new MessageElement( "Вид деятельности компании?", "", "", [
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['restor']),
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['obraz'])
                                                    ])
                                                ]
                                            ]
                                        ));
                                        $lib->setLastAction($message['sender']['id'], json_encode(array('name' => 'activity')));
                                    } else {
                                        $lib->setLastAction($message['sender']['id'], "");
                                        $lib->sendMail($message['sender']['id']);
                                        $bot->send(new Message($message['sender']['id'], $l['aleksandr2']));
                                        $bot->send(new StructuredMessage($message['sender']['id'],
                                            StructuredMessage::TYPE_GENERIC,
                                            [
                                                'elements' => [
                                                    new MessageElement( 'Меню', "", "", [
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['whatCan']),
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['order'])
                                                    ]),
                                                    new MessageElement( 'Меню', "", "", [
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['price']),
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['callMe'])
                                                    ])
                                                ]
                                            ]
                                        ));
                                    }
                                    $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_OFF));
                                break;
                            }
                        } else {
                            $text = "Postback received: ".trim($message['postback']['payload']);
                            $bot->send(new Message($message['sender']['id'], $text));
                        }
                    break;
                }


                continue;
            }
            // Handle command
            switch ($command) {

                case "/start":
                    $bot->send(new StructuredMessage($message['sender']['id'],
                        StructuredMessage::TYPE_GENERIC,
                        [
                            'elements' => [
                                new MessageElement( 'Меню', "", "", [
                                    new MessageButton(MessageButton::TYPE_POSTBACK, $l['whatCan']),
                                    new MessageButton(MessageButton::TYPE_POSTBACK, $l['order'])
                                ]),
                                new MessageElement( 'Меню', "", "", [
                                    new MessageButton(MessageButton::TYPE_POSTBACK, $l['price']),
                                    new MessageButton(MessageButton::TYPE_POSTBACK, $l['callMe'])
                                ])
                            ]
                        ]
                    ));
                break;

                default:
                    if (!empty($command)){
                        $action = $lib->getLastAction($message['sender']['id']);

                        if ($action == 'number') {
                            $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_ON));

                            $lib->setUserInfo($message['sender']['id'], $action, $command);

                            $bot->send(new StructuredMessage($message['sender']['id'],
                                StructuredMessage::TYPE_BUTTON,
                                [
                                    'text' => "И email, будьте добры!",
                                    'buttons' => [
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['goBrif']),
                                        new MessageButton(MessageButton::TYPE_POSTBACK, $l['moreQuestions']),
                                    ]
                                ]
                            ));
                            $bot->send(new Message($message['sender']['id'], $l['goemail']));
                            $lib->setLastAction($message['sender']['id'], 'email');

                            $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_OFF));
                        }

                        elseif ($action == "number_start") {
                            $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_ON));
                            $lib->setUserInfo($message['sender']['id'], "number", $command);
                            $bot->send(new StructuredMessage($message['sender']['id'],
                                StructuredMessage::TYPE_GENERIC,
                                [
                                    'elements' => [
                                        new MessageElement("И email.", "", "", [
                                            new MessageButton(MessageButton::TYPE_POSTBACK, $l['whatCan']),
                                            new MessageButton(MessageButton::TYPE_POSTBACK, $l['order'])
                                        ]),
                                        new MessageElement("И email.", "", "", [
                                            new MessageButton(MessageButton::TYPE_POSTBACK, $l['price']),
                                            new MessageButton(MessageButton::TYPE_POSTBACK, $l['callMe'])
                                        ])
                                    ]
                                ]
                            ));

                            $lib->setLastAction($message['sender']['id'], 'email_start');
                            $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_OFF));
                        }

                        elseif ($action == "email_start"){
                            $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_ON));
                            $lib->setUserInfo($message['sender']['id'], 'email', $command);
                            $bot->send(new Message($message['sender']['id'], "Спасибо! Ваши контакты заботливо сохранены!"));
                            $lib->setLastAction($message['sender']['id'], ' ');
                            $bot->send(new Message($message['sender']['id'], $l['start']));
                            $bot->send(new StructuredMessage($message['sender']['id'],
                                StructuredMessage::TYPE_GENERIC,
                                [
                                    'elements' => [
                                        new MessageElement( 'Меню', "", "", [
                                            new MessageButton(MessageButton::TYPE_POSTBACK, $l['whatCan']),
                                            new MessageButton(MessageButton::TYPE_POSTBACK, $l['order'])
                                        ]),
                                        new MessageElement( 'Меню', "", "", [
                                            new MessageButton(MessageButton::TYPE_POSTBACK, $l['price']),
                                            new MessageButton(MessageButton::TYPE_POSTBACK, $l['callMe'])
                                        ])
                                    ]
                                ]
                            ));
                            $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_OFF));
                        }

                        else {
                            $bot->send(new Message($message['sender']['id'], 'Что-что-что?'));
                        }

                    }
            }
        }
    }
}